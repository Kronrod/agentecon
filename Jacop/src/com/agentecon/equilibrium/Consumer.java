package com.agentecon.equilibrium;

import org.jacop.core.Store;
import org.jacop.floats.constraints.PeqC;
import org.jacop.floats.constraints.PeqQ;
import org.jacop.floats.constraints.PmulQeqR;
import org.jacop.floats.constraints.PplusCeqR;
import org.jacop.floats.constraints.PplusQeqR;
import org.jacop.floats.core.FloatVar;

public class Consumer implements IConsumer {

	private Store store;
	private String type;

	private double[] weights;
	private FloatVar[] inputs;
	private FloatVar work;
	private FloatVar leisure;
	private double leisureWeight;

	public Consumer(Store store, String type, double hoursPerDay, double leisureWeight, double... inputWeights) {
		this.type = type;
		this.store = store;
		this.weights = inputWeights;
		this.leisureWeight = leisureWeight;
		this.inputs = new FloatVar[inputWeights.length];
		for (int i = 0; i < inputs.length; i++) {
			inputs[i] = new FloatVar(store, 0.0, Double.MAX_VALUE);
		}
		this.work = new FloatVar(store, type + "_work", 0.0, hoursPerDay);
		this.leisure = new FloatVar(store, type + "_leisure", 0.0, hoursPerDay);

		FloatVar sum = new FloatVar(store, 0.0, Double.MAX_VALUE);
		store.impose(new PplusQeqR(work, leisure, sum));
		store.impose(new PeqC(sum, hoursPerDay));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.agentecon.equilibrium.IConsumer#imposeConstraints(org.jacop.floats.core.FloatVar, org.jacop.floats.core.FloatVar)
	 */
	static boolean first = true;

	@Override
	public void imposeConstraints(FloatVar dividend, FloatVar wage, FloatVar... goodsPrices) {
		if (first) {
			first = false;
		} else {
			imposeBudget(dividend, wage, goodsPrices);
		}
		FloatVar lambda = new FloatVar(store, 0.0, Double.MAX_VALUE);
		imposeLabor(lambda, wage);
		for (int i = 0; i < inputs.length; i++) {
			imposeInput(lambda, inputs[i], goodsPrices[i], weights[i]);
		}
	}

	private void imposeInput(FloatVar lambda, FloatVar input, FloatVar price, double weight) {
		FloatVar oneup = new FloatVar(store, 0.0, Double.MAX_VALUE);
		store.impose(new PplusCeqR(input, 1.0, oneup));

		FloatVar temp1 = new FloatVar(store, 0.0, Double.MAX_VALUE);
		store.impose(new PmulQeqR(oneup, price, temp1));
		FloatVar left = new FloatVar(store, 0.0, Double.MAX_VALUE);
		store.impose(new PmulQeqR(lambda, temp1, left));
		store.impose(new PeqC(left, weight));
	}

	private void imposeLabor(FloatVar lambda, FloatVar wage) {
		FloatVar oneup = new FloatVar(store, 0.0, Double.MAX_VALUE);
		store.impose(new PplusCeqR(leisure, 1.0, oneup));
		
		FloatVar temp1 = new FloatVar(store, 0.0, Double.MAX_VALUE);
		store.impose(new PmulQeqR(wage, oneup, temp1));
		FloatVar left = new FloatVar(store, 0.0, Double.MAX_VALUE);
		store.impose(new PmulQeqR(lambda, temp1, left));
		store.impose(new PeqC(left, leisureWeight));
	}

	private void imposeBudget(FloatVar dividend, FloatVar wage, FloatVar[] goodsPrices) {
		FloatVar income = new FloatVar(store, 0.0, Double.MAX_VALUE);
		FloatVar salary = new FloatVar(store, 0.0, Double.MAX_VALUE);
		store.impose(new PmulQeqR(work, wage, salary));
		store.impose(new PplusQeqR(dividend, salary, income));

		FloatVar costs = null;
		for (int i = 0; i < goodsPrices.length; i++) {
			FloatVar mul = new FloatVar(store, 0.0, Double.MAX_VALUE);
			store.impose(new PmulQeqR(goodsPrices[i], inputs[i], mul));
			if (costs == null) {
				costs = mul;
			} else {
				FloatVar sum = new FloatVar(store, 0.0, Double.MAX_VALUE);
				store.impose(new PplusQeqR(costs, mul, sum));
				costs = sum;
			}
		}
		store.impose(new PeqQ(income, costs));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.agentecon.equilibrium.IConsumer#getConsumption()
	 */
	@Override
	public FloatVar[] getConsumption() {
		return inputs;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.agentecon.equilibrium.IConsumer#getWorkHours()
	 */
	@Override
	public FloatVar getWorkHours() {
		return work;
	}

	@Override
	public String getType() {
		return type;
	}

}
