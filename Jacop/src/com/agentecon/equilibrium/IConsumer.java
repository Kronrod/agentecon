package com.agentecon.equilibrium;

import org.jacop.floats.core.FloatVar;

public interface IConsumer {

	public void imposeConstraints(FloatVar dividend, FloatVar wage, FloatVar... goodsPrices);

	public FloatVar[] getConsumption();

	public FloatVar getWorkHours();

	public String getType();

}