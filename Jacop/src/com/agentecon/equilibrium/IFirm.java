package com.agentecon.equilibrium;

import org.jacop.floats.core.FloatVar;

public interface IFirm {
	
	public void imposeConstraints(FloatVar outputPrice, FloatVar... inputPrices);
	
	public FloatVar getDividend();
	
	public FloatVar getOutput();

	public FloatVar[] getInputs();

	public String getType();

}
