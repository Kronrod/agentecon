package com.agentecon.equilibrium;

import org.jacop.core.Store;
import org.jacop.floats.constraints.LnPeqR;
import org.jacop.floats.constraints.PeqQ;
import org.jacop.floats.constraints.PminusQeqR;
import org.jacop.floats.constraints.PmulCeqR;
import org.jacop.floats.constraints.PmulQeqR;
import org.jacop.floats.constraints.PplusCeqR;
import org.jacop.floats.constraints.PplusQeqR;
import org.jacop.floats.core.FloatVar;

public class LogProdFirm implements IFirm {

	private Store store;

	private String type;
	private double[] weights;
	private FloatVar rawDividend;
	private FloatVar rawOutput;
	private FloatVar[] rawInputs;

	public LogProdFirm(Store store, String type, double... inputWeights) {
		this.store = store;
		this.type = type;
		this.weights = inputWeights;

		this.rawDividend = new FloatVar(store, type + "_dividend", 0.0, Double.MAX_VALUE);
		this.rawOutput = new FloatVar(store, type + "_output", 0.0, Double.MAX_VALUE);

		this.rawInputs = new FloatVar[inputWeights.length];
		for (int i = 0; i < inputWeights.length; i++) {
			this.rawInputs[i] = new FloatVar(store, type + "-input-" + i, 0.0, Double.MAX_VALUE);
		}
	}

	public void imposeConstraints(FloatVar outputPrice, FloatVar... inputPrices) {
		for (int i = 0; i < inputPrices.length; i++) {
			imposeMax(outputPrice, weights[i], rawInputs[i], inputPrices[i]);
		}
		calcOutput();
		calcDividend(outputPrice, inputPrices);
	}

	private void calcDividend(FloatVar outputPrice, FloatVar[] inputPrices) {
		FloatVar costs = null;
		for (int i = 0; i < inputPrices.length; i++) {
			FloatVar mul = new FloatVar(store, 0.0, Double.MAX_VALUE);
			store.impose(new PmulQeqR(inputPrices[i], rawInputs[i], mul));
			if (costs == null) {
				costs = mul;
			} else {
				FloatVar sum = new FloatVar(store, 0.0, Double.MAX_VALUE);
				store.impose(new PplusQeqR(costs, mul, sum));
				costs = sum;
			}
		}
		FloatVar revenue = new FloatVar(store, 0.0, Double.MAX_VALUE);
		store.impose(new PmulQeqR(outputPrice, rawOutput, revenue));
		store.impose(new PminusQeqR(revenue, costs, rawDividend));
	}

	private void calcOutput() {
		FloatVar output = null;
		for (int i = 0; i < rawInputs.length; i++) {
			FloatVar oneup = new FloatVar(store, 0.0, Double.MAX_VALUE);
			store.impose(new PplusCeqR(rawInputs[i], 1.0, oneup));
			FloatVar log = new FloatVar(store, 0.0, 100);
			store.impose(new LnPeqR(oneup, log));
			FloatVar weighted = new FloatVar(store, 0.0, Double.MAX_VALUE);
			store.impose(new PmulCeqR(log, weights[i], weighted));
			if (output == null) {
				output = weighted;
			} else {
				FloatVar sum = new FloatVar(store, 0.0, Double.MAX_VALUE);
				store.impose(new PplusQeqR(output, weighted, sum));
				output = sum;
			}
		}
		store.impose(new PplusCeqR(output, 1.0, rawOutput));
	}

	private void imposeMax(FloatVar outputPrice, double weight, FloatVar amount, FloatVar inputPrice) {
		FloatVar right = new FloatVar(store, 0.0, Double.MAX_VALUE);
		store.impose(new PmulCeqR(outputPrice, weight, right));

		FloatVar oneup = new FloatVar(store, 0.0, Double.MAX_VALUE);
		store.impose(new PplusCeqR(amount, 1.0, oneup));
		FloatVar left = new FloatVar(store, 0.0, Double.MAX_VALUE);
		store.impose(new PmulQeqR(oneup, inputPrice, left));

		store.impose(new PeqQ(left, right));
	}

	public FloatVar getDividend() {
		return rawDividend;
	}

	public FloatVar getOutput() {
		return rawOutput;
	}

	public FloatVar[] getInputs() {
		return rawInputs;
	}

	public String getType() {
		return type;
	}

}
