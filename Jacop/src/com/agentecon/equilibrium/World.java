package com.agentecon.equilibrium;

import java.util.ArrayList;
import java.util.Arrays;

import org.jacop.core.Store;
import org.jacop.floats.constraints.PeqC;
import org.jacop.floats.constraints.PeqQ;
import org.jacop.floats.constraints.PmulCeqR;
import org.jacop.floats.constraints.PplusQeqR;
import org.jacop.floats.core.FloatVar;
import org.jacop.floats.search.SplitSelectFloat;
import org.jacop.search.DepthFirstSearch;
import org.jacop.search.PrintOutListener;

public class World {

	protected Store store;
	protected ArrayList<IFirm> firms;
	private ArrayList<IConsumer> consumers;

	private FloatVar dividend;
	private FloatVar[] inputPrices;
	private FloatVar[] outputPrices;

	public World(double leisureWeight, double[] consumptionWeights, double consumerCount, double[] productionWeights, double firmCount) {
		this(leisureWeight, consumptionWeights, consumerCount, productionWeights, firmCount, null);
	}

	public World(double leisureWeight, double[] consumptionWeights, double consumerCount, double[] productionWeights, double firmCount, double[] priceHints) {
		this.store = new Store();
		this.consumers = new ArrayList<IConsumer>();
		this.firms = new ArrayList<IFirm>();

		this.inputPrices = new FloatVar[productionWeights.length];
		for (int i = 0; i < productionWeights.length; i++) {
			this.inputPrices[i] = new FloatVar(store, "wage_" + i, 0.0, Double.MAX_VALUE);
		}
		this.outputPrices = new FloatVar[consumptionWeights.length];
		for (int i = 0; i < consumptionWeights.length; i++) {
			this.outputPrices[i] = new FloatVar(store, "price_" + i, 0.0, Double.MAX_VALUE);
		}
		this.dividend = new FloatVar(store, "dividends", 0.0, Double.MAX_VALUE);

		for (int i = 0; i < consumptionWeights.length; i++) {
			this.addFirmType("firm_" + i, firmCount, rotate(productionWeights, i));
		}
		for (int i = 0; i < productionWeights.length; i++) {
			this.addConsumerType("consumer_" + i, consumerCount, leisureWeight, rotate(consumptionWeights, i));
		}

		imposeConstraints();

		System.out.println("Running constrained optimization with " + store.variablesHashMap.size() + " variables");
	}

	private double[] rotate(double[] productionWeights, int i) {
		int len = productionWeights.length;
		i = i % len;
		double[] rotated = new double[len];
		System.arraycopy(productionWeights, 0, rotated, len - i, i);
		System.arraycopy(productionWeights, i, rotated, 0, len - i);
		return rotated;
	}

	private void imposeConstraints() {
		store.impose(new PeqC(outputPrices[0], 1.0)); // normalize a price to 1

		for (int i = 0; i < firms.size(); i++) {
			firms.get(i).imposeConstraints(outputPrices[i], inputPrices);
		}

		FloatVar dividendFraction = new FloatVar(store, 0.0, Double.MAX_VALUE);
		store.impose(new PmulCeqR(dividendFraction, consumers.size(), dividend));
		for (int i = 0; i < consumers.size(); i++) {
			consumers.get(i).imposeConstraints(dividendFraction, inputPrices[i], outputPrices);
		}

		wireLabor();
		wireOutputsToConsumption();
		wireDividend();
	}

	private void wireLabor() {
		for (int i = 0; i < consumers.size(); i++) {
			final int j = i;
			FloatVar sum = sum(firms, new IVarSelector<IFirm>() {

				@Override
				public FloatVar extract(IFirm con) {
					return con.getInputs()[j];
				}

			});
			store.impose(new PeqQ(sum, consumers.get(i).getWorkHours()));
		}
	}

	public FloatVar getOutput(int i) {
		return firms.get(i).getOutput();
	}

	private void wireDividend() {
		store.impose(new PeqQ(dividend, sum(firms, new IVarSelector<IFirm>() {

			@Override
			public FloatVar extract(IFirm con) {
				return con.getDividend();
			}
		})));
	}

	private void wireOutputsToConsumption() {
		for (int i = 0; i < firms.size(); i++) {
			final int j = i;
			FloatVar sum = sum(consumers, new IVarSelector<IConsumer>() {

				@Override
				public FloatVar extract(IConsumer con) {
					return con.getConsumption()[j];
				}

			});
			store.impose(new PeqQ(sum, firms.get(i).getOutput()));
		}
	}

	private <T> FloatVar sum(ArrayList<T> list, IVarSelector<T> selector) {
		FloatVar sum = null;
		for (T con : list) {
			FloatVar var = selector.extract(con);
			if (sum == null) {
				sum = var;
			} else {
				FloatVar newsum = new FloatVar(store, 0.0, Double.MAX_VALUE);
				store.impose(new PplusQeqR(var, sum, newsum));
				sum = newsum;
			}
		}
		return sum;
	}

	private void addConsumerType(String string, double count, double leisureWeight, double... weights) {
		IConsumer cons = new Consumer(store, string, 24, leisureWeight, weights);
		consumers.add(new ScaledConsumer(store, cons, count));
	}

	protected void addFirmType(String name, double count, double... inputWeights) {
		LogProdFirm firm = new LogProdFirm(store, name, inputWeights);
		firms.add(new ScaledFirm(store, firm, count));
	}

	public void solve() {
		DepthFirstSearch<FloatVar> search = new DepthFirstSearch<FloatVar>();
		ArrayList<FloatVar> all = new ArrayList<FloatVar>();
		all.addAll(Arrays.asList(inputPrices));
		all.addAll(Arrays.asList(outputPrices));
		all.add(dividend);
		for (IConsumer c : consumers) {
			all.add(c.getWorkHours());
		}
		for (IConsumer c : consumers) {
			all.addAll(Arrays.asList(c.getConsumption()));
		}
		SplitSelectFloat<FloatVar> s = new SplitSelectFloat<FloatVar>(store, all.toArray(new FloatVar[] {}), null);

		search.setSolutionListener(new PrintOutListener<FloatVar>());

		search.getSolutionListener().searchAll(true);
		search.labeling(store, s);
		System.out.println(store.variablesHashMap.size() + " variables");
	}

	public static void main(String[] args) {
		long t0 = System.nanoTime();
		try {
			// No of solutions : 644
			// Last Solution : [wage_0=0.98907387892, wage_1=0.89388981411, price_0=1.00000000000, price_1=0.42889561114, dividends=113.82108557945]
			new World(14.0, new double[] { 8.0, 2.0 }, 1, new double[] { 6.0, 4.0 }, 1).solve(); // , 4.0, 8.0
		} finally {
			long diff = (System.nanoTime() - t0) / 1000000;
			System.out.println(diff + "ms");
		}
	}

}
