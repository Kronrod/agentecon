package com.agentecon.equilibrium;

import java.util.ArrayList;

import org.jacop.core.Store;
import org.jacop.floats.constraints.ExpPeqR;
import org.jacop.floats.constraints.LnPeqR;
import org.jacop.floats.constraints.PeqC;
import org.jacop.floats.constraints.PmulQeqR;
import org.jacop.floats.core.FloatVar;
import org.jacop.floats.search.SplitSelectFloat;
import org.jacop.search.DepthFirstSearch;
import org.jacop.search.PrintOutListener;

public class LogTest {

	private Store store = new Store();
	private ArrayList<FloatVar> all = new ArrayList<FloatVar>();

	public LogTest(double a, double b) {
		FloatVar avar = new FloatVar(store, 0, Double.MAX_VALUE);
		store.impose(new PeqC(avar, a));

		FloatVar bvar = new FloatVar(store, 0, Double.MAX_VALUE);
		store.impose(new PeqC(bvar, b));

		all.add(power(avar, bvar));
	}

	private FloatVar power(FloatVar a, FloatVar exp) {
		FloatVar result = new FloatVar(store, 0, 10);
		FloatVar log = new FloatVar(store, -10, 10);
		store.impose(new LnPeqR(a, log));
		FloatVar mul = new FloatVar(store, -10, 10);
		store.impose(new PmulQeqR(log, exp, mul));
		store.impose(new ExpPeqR(mul, result));
		return result;
	}

	public void solve() {
		DepthFirstSearch<FloatVar> search = new DepthFirstSearch<FloatVar>();
		SplitSelectFloat<FloatVar> s = new SplitSelectFloat<FloatVar>(store, all.toArray(new FloatVar[] {}), null);

		search.setSolutionListener(new PrintOutListener<FloatVar>());

		search.getSolutionListener().searchAll(true);
		search.labeling(store, s);
	}

	public static void main(String[] args) {
		LogTest test = new LogTest(0.25, 0.5);
		test.solve();
	}

}
