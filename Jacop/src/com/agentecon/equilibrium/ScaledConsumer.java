package com.agentecon.equilibrium;

import org.jacop.core.Store;
import org.jacop.floats.constraints.PmulCeqR;
import org.jacop.floats.core.FloatVar;

public class ScaledConsumer implements IConsumer {

	private Store store;
	private IConsumer wrapped;

	private double scale;
	private FloatVar labor;
	private FloatVar[] consumption;

	public ScaledConsumer(Store store, IConsumer wrapped, double size) {
		this.wrapped = wrapped;
		this.store = store;
		this.scale = size;

		this.labor = new FloatVar(store, wrapped.getType() + "_tot_labor", 0.0, Double.MAX_VALUE);
		store.impose(new PmulCeqR(wrapped.getWorkHours(), size, labor));

		FloatVar[] inputs = wrapped.getConsumption();
		this.consumption = new FloatVar[inputs.length];
		String type = wrapped.getType();
		for (int i = 0; i < inputs.length; i++) {
			this.consumption[i] = new FloatVar(store, type + "_tot_input_" + i, 0.0, Double.MAX_VALUE);
			store.impose(new PmulCeqR(inputs[i], size, consumption[i]));
		}
	}

	@Override
	public void imposeConstraints(FloatVar dividend, FloatVar wage, FloatVar... goodsPrices) {
		FloatVar scaledDividend = new FloatVar(store, 0.0, Double.MAX_VALUE);
		store.impose(new PmulCeqR(scaledDividend, scale, dividend));
		this.wrapped.imposeConstraints(scaledDividend, wage, goodsPrices);
	}

	@Override
	public FloatVar[] getConsumption() {
		return consumption;
	}

	@Override
	public FloatVar getWorkHours() {
		return labor;
	}

	@Override
	public String getType() {
		return wrapped.getType();
	}

}
