package com.agentecon.equilibrium;

import org.jacop.core.Store;
import org.jacop.floats.constraints.LnPeqR;
import org.jacop.floats.constraints.PeqC;
import org.jacop.floats.constraints.PmulQeqR;
import org.jacop.floats.constraints.PplusCeqR;
import org.jacop.floats.constraints.PplusQeqR;
import org.jacop.floats.core.FloatVar;
import org.jacop.floats.search.SplitSelectFloat;
import org.jacop.search.DepthFirstSearch;
import org.jacop.search.PrintOutListener;

public class Simple {

	private Store store;
	private FloatVar hours;

	public Simple() {
		this.store = new Store();
		this.hours = new FloatVar(store, "hours", 0.0, 24.0);
		
		FloatVar hplusone = new FloatVar(store, 0.0, 100.0);
		store.impose(new PplusCeqR(hours, 1.0, hplusone));
		
		FloatVar logone = new FloatVar(store, 0.0, 10.0);
		store.impose(new LnPeqR(hplusone, logone));

		FloatVar temp = new FloatVar(store, 2.0, 12.0);
		store.impose(new PplusCeqR(logone, 2.0, temp));

		FloatVar leftside = new FloatVar(store, 0.0, 100.0);
		store.impose(new PmulQeqR(temp, hplusone, leftside));

		FloatVar left2 = new FloatVar(store, 0.0, 200.0);
		store.impose(new PplusQeqR(leftside, hours, left2));
		store.impose(new PeqC(left2, 24.0));
	}

	public void solve() {
		DepthFirstSearch<FloatVar> search = new DepthFirstSearch<FloatVar>();
		SplitSelectFloat<FloatVar> s = new SplitSelectFloat<FloatVar>(store, new FloatVar[] { hours }, null);

		search.setSolutionListener(new PrintOutListener<FloatVar>());
		search.getSolutionListener().searchAll(true);
		search.labeling(store, s);

	}

	public static void main(String[] args) {
		new Simple().solve();
	}

}
