package com.agentecon.equilibrium;

public class CobbDougWorld extends World {

	public CobbDougWorld(double leisureWeight, double[] consumptionWeights, double consumerCount, double[] productionWeights, double firmCount) {
		super(leisureWeight, consumptionWeights, consumerCount, productionWeights, firmCount);
	}
	
	@Override
	protected void addFirmType(String name, double count, double... inputWeights) {
		CobbDouglasFirm firm = new CobbDouglasFirm(store, name, 10.0, inputWeights);
		firms.add(new ScaledFirm(store, firm, count));
	}
	
	public static void main(String[] args) {
		long t0 = System.nanoTime();
		try {
			new CobbDougWorld(10.0, new double[] { 5.0 }, 100, new double[] { 0.5 }, 10).solve(); // , 4.0, 8.0
		} finally {
			long diff = (System.nanoTime() - t0)/1000000;
			System.out.println(diff + "ms");
		}
	}

}
