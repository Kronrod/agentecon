package com.agentecon.equilibrium;

import java.util.ArrayList;

import org.jacop.core.Store;
import org.jacop.floats.constraints.PeqC;
import org.jacop.floats.constraints.PmulCeqR;
import org.jacop.floats.constraints.PplusQeqR;
import org.jacop.floats.core.FloatVar;
import org.jacop.floats.search.SplitSelectFloat;
import org.jacop.search.DepthFirstSearch;
import org.jacop.search.PrintOutListener;

public class TwoByTwoModel {

	private Store store;
	private IConsumer italian, swiss;
	private IFirm pizzeria, chalet;

	private FloatVar dividend;
	private FloatVar italianWage, swissWage;
	private FloatVar pizzaPrice, fonduePrice;

	public TwoByTwoModel(double leisureWeight, double[] consumptionWeights, double[] productionWeights) {
		this.store = new Store();

		this.italianWage = new FloatVar(store, "italian_wage", 0.2, 0.24);
		this.swissWage = new FloatVar(store, "swiss_wage", 0.2, 0.24);

		this.pizzaPrice = new FloatVar(store, "pizza_price", 1.0, 1.0);
		this.fonduePrice = new FloatVar(store, "fondue_price", 0.3, 0.32);
		
//		this.italianWage = new FloatVar(store, "italian_wage", 0.0, Double.MAX_VALUE);
//		this.swissWage = new FloatVar(store, "swiss_wage", 0.0, Double.MAX_VALUE);
//
//		this.pizzaPrice = new FloatVar(store, "pizza_price", 0.0, Double.MAX_VALUE);
//		this.fonduePrice = new FloatVar(store, "fondue_price", 0.0, Double.MAX_VALUE);

		this.dividend = new FloatVar(store, "dividends", 0.0, Double.MAX_VALUE);

		this.pizzeria = new ScaledFirm(store, new LogProdFirm(store, "pizzeria", productionWeights), 10);
		this.chalet = new ScaledFirm(store, new LogProdFirm(store, "chalet", new double[]{productionWeights[1], productionWeights[0]}), 10);
		
//		store.impose(new PeqQ(pizzaPrice, fonduePrice));
//		store.impose(new PeqQ(italianWage, swissWage));
		
		this.italian = new ScaledConsumer(store, new Consumer(store, "Italian", 24, leisureWeight, consumptionWeights), 100);
		this.swiss = new ScaledConsumer(store, new Consumer(store, "Swiss", 24, leisureWeight, consumptionWeights), 100);

		imposeConstraints();
	}

	private void imposeConstraints() {
		store.impose(new PeqC(pizzaPrice, 1.0)); // normalize a price to 1
		
		pizzeria.imposeConstraints(pizzaPrice, italianWage, swissWage);
		chalet.imposeConstraints(fonduePrice, italianWage, swissWage);
		
		FloatVar dividendFraction = new FloatVar(store, 0.0, Double.MAX_VALUE);
		store.impose(new PmulCeqR(dividendFraction, 2, dividend));
		italian.imposeConstraints(dividendFraction, italianWage, pizzaPrice, fonduePrice);
		swiss.imposeConstraints(dividendFraction, swissWage, pizzaPrice, fonduePrice);
		
		wireLabor();
		wireOutputsToConsumption();
		wireDividend();
	}

	private void wireLabor() {
		store.impose(new PplusQeqR(pizzeria.getInputs()[0], chalet.getInputs()[0], italian.getWorkHours()));
		store.impose(new PplusQeqR(pizzeria.getInputs()[1], chalet.getInputs()[1], swiss.getWorkHours()));
	}

	private void wireDividend() {
		store.impose(new PplusQeqR(pizzeria.getDividend(), chalet.getDividend(), dividend));
	}

	private void wireOutputsToConsumption() {
		store.impose(new PplusQeqR(italian.getConsumption()[0], swiss.getConsumption()[0], pizzeria.getOutput()));
		store.impose(new PplusQeqR(italian.getConsumption()[1], swiss.getConsumption()[1], chalet.getOutput()));
	}

	public void solve() {
		DepthFirstSearch<FloatVar> search = new DepthFirstSearch<FloatVar>();
		ArrayList<FloatVar> all = new ArrayList<FloatVar>();
		all.add(pizzaPrice);
		all.add(fonduePrice);
		all.add(swissWage);
		all.add(italianWage);
		all.add(dividend);
		all.add(pizzeria.getOutput());
		all.add(chalet.getOutput());
		all.add(swiss.getWorkHours());
		all.add(italian.getWorkHours());
		SplitSelectFloat<FloatVar> s = new SplitSelectFloat<FloatVar>(store, all.toArray(new FloatVar[] {}), null);

		search.setSolutionListener(new PrintOutListener<FloatVar>());
		search.getSolutionListener().searchAll(true);
		search.labeling(store, s);
	}

	public static void main(String[] args) {
		new TwoByTwoModel(14.0, new double[] { 8.0, 2.0 }, new double[] { 6.0, 4.0 }).solve();
	}

}
