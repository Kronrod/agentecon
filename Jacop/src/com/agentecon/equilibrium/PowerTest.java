package com.agentecon.equilibrium;

import java.util.Random;

public class PowerTest {

	private static final int ITERATIONS = 10000000;

	private Random rand = new Random(13);

	public void runPower() {
		for (int i = 0; i < ITERATIONS; i++) {
			Math.pow(rand.nextDouble(), rand.nextDouble());
		}
	}

	public void runLogs() {
		for (int i = 0; i < ITERATIONS; i++) {
			Math.log(rand.nextDouble() + rand.nextDouble());
		}
	}

	public static void main(String[] args) {
		PowerTest test = new PowerTest();
		for (int i = 0; i < 5; i++) {
			{
				long t0 = System.nanoTime();
				test.runPower();
				long t1 = System.nanoTime();
				System.out.println((t1 - t0) / 1000000 + "ms for pow");
			}
			{
				long t0 = System.nanoTime();
				test.runPower();
				long t1 = System.nanoTime();
				System.out.println((t1 - t0) / 1000000 + "ms for log");
			}
		}
	}

}
