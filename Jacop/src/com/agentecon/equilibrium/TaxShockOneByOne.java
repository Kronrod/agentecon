package com.agentecon.equilibrium;

import java.util.ArrayList;

import org.jacop.core.Store;
import org.jacop.floats.constraints.LnPeqR;
import org.jacop.floats.constraints.PeqC;
import org.jacop.floats.constraints.PeqQ;
import org.jacop.floats.constraints.PminusQeqR;
import org.jacop.floats.constraints.PmulCeqR;
import org.jacop.floats.constraints.PmulQeqR;
import org.jacop.floats.constraints.PplusCeqR;
import org.jacop.floats.constraints.PplusQeqR;
import org.jacop.floats.core.FloatVar;
import org.jacop.floats.search.SplitSelectFloat;
import org.jacop.search.DepthFirstSearch;
import org.jacop.search.PrintOutListener;

public class TaxShockOneByOne {

	private Store store;

	private FloatVar x1, x2, y1, y2, util;

	public TaxShockOneByOne(double leisureWeight, double consWeight, double consumersPerFirm, double prodWeight, double tax) {
		this.store = new Store();

		// this.pizzaPrice = new FloatVar(store, "pizza_price", 0.0, Double.MAX_VALUE);
		// this.fonduePrice = new FloatVar(store, "fondue_price", 0.0, Double.MAX_VALUE);

		this.x1 = new FloatVar(store, "x1", 0.0, Double.MAX_VALUE);
		this.x2 = new FloatVar(store, "x2", 0.0, Double.MAX_VALUE);
		this.y1 = new FloatVar(store, "y1", 0.0, Double.MAX_VALUE);
		this.y2 = new FloatVar(store, "y2", 0.0, Double.MAX_VALUE);

		FloatVar prod1 = createProdFun(x1, consumersPerFirm, prodWeight);
		store.impose(new PeqQ(prod1, y1));

		FloatVar prod2 = createProdFun(x2, consumersPerFirm, prodWeight);
		store.impose(new PmulCeqR(prod2, tax, y2));

		FloatVar sum1 = new FloatVar(store, 0.0, Double.MAX_VALUE);
		store.impose(new PplusQeqR(y1, y2, sum1));
		FloatVar sum2 = new FloatVar(store, 0.0, Double.MAX_VALUE);
		store.impose(new PplusCeqR(sum1, 1.0, sum2));
		FloatVar brack1 = new FloatVar(store, 0.0, Double.MAX_VALUE);
		store.impose(new PmulCeqR(sum2, leisureWeight / (2 * consWeight) * consumersPerFirm / prodWeight, brack1));
		FloatVar brack2 = new FloatVar(store, 0.0, Double.MAX_VALUE);
		store.impose(new PmulCeqR(brack1, 1.0 / tax, brack2));

		createEquation(brack1, x1, consumersPerFirm);
		createEquation(brack2, x2, consumersPerFirm);
		
		util = new FloatVar(store, "Utility", 0.0, Double.MAX_VALUE);
		FloatVar mul1 = createUtilTerm(leisureWeight / 2, x1);
		FloatVar mul2 = createUtilTerm(leisureWeight / 2, x2);
		FloatVar cons = new FloatVar(store, 0.0, Double.MAX_VALUE);
		store.impose(new PmulCeqR(sum2, 0.5, cons));
		FloatVar oneup = new FloatVar(store, 0.0, Double.MAX_VALUE);
		store.impose(new PplusCeqR(cons, 0.5, oneup));
		FloatVar log = new FloatVar(store, 0.0, Double.MAX_VALUE);
		store.impose(new LnPeqR(oneup, log));
		FloatVar mul3 = new FloatVar(store, 0.0, Double.MAX_VALUE);
		store.impose(new PmulCeqR(log, consWeight * 2 / 2, mul3));
		FloatVar s1 = new FloatVar(store, 0.0, Double.MAX_VALUE);
		store.impose(new PplusQeqR(mul1, mul2, s1));
		store.impose(new PplusQeqR(s1, mul3, util));
	}

	private FloatVar createUtilTerm(double weight, FloatVar x) {
		FloatVar twentyFive = new FloatVar(store, 0.0, Double.MAX_VALUE);
		store.impose(new PeqC(twentyFive, 25.0));
		FloatVar sum = new FloatVar(store, 0.0, Double.MAX_VALUE);
		store.impose(new PminusQeqR(twentyFive, x, sum));
		FloatVar log = new FloatVar(store, 0.0, Double.MAX_VALUE);
		store.impose(new LnPeqR(sum, log));
		FloatVar mul = new FloatVar(store, 0.0, Double.MAX_VALUE);
		store.impose(new PmulCeqR(log, weight, mul));
		return mul;
	}

	private void createEquation(FloatVar brack1, FloatVar x1, double consumersPerFirm) {
		FloatVar mul1 = new FloatVar(store, 0.0, Double.MAX_VALUE);
		store.impose(new PmulCeqR(x1, consumersPerFirm, mul1));
		FloatVar oneup = new FloatVar(store, 0.0, Double.MAX_VALUE);
		store.impose(new PplusCeqR(mul1, 1.0, oneup));
		
		FloatVar mul2 = new FloatVar(store, 0.0, Double.MAX_VALUE);
		store.impose(new PmulQeqR(brack1, oneup, mul2));
		
		FloatVar add1 = new FloatVar(store, 0.0, Double.MAX_VALUE);
		store.impose(new PplusQeqR(mul1, x1, add1));
		store.impose(new PeqC(add1, 25.0));
	}

	private FloatVar createProdFun(FloatVar x1, double consPerFirm, double prodWeight) {
		FloatVar scaledX = new FloatVar(store, 0.0, Double.MAX_VALUE);
		store.impose(new PmulCeqR(x1, consPerFirm, scaledX));
		FloatVar oneup = new FloatVar(store, 0.0, Double.MAX_VALUE);
		store.impose(new PplusCeqR(scaledX, 1.0, oneup));
		FloatVar log = new FloatVar(store, 0.0, Double.MAX_VALUE);
		store.impose(new LnPeqR(oneup, log));
		FloatVar weighted = new FloatVar(store, 0.0, Double.MAX_VALUE);
		store.impose(new PmulCeqR(log, prodWeight, weighted));

		FloatVar output = new FloatVar(store, 0.0, Double.MAX_VALUE);
		store.impose(new PplusCeqR(weighted, 1.0, output));

		FloatVar scaled = new FloatVar(store, 0.0, Double.MAX_VALUE);
		store.impose(new PmulCeqR(output, 1.0 / consPerFirm, scaled));
		return scaled;
	}

	public void solve() {
		DepthFirstSearch<FloatVar> search = new DepthFirstSearch<FloatVar>();
		ArrayList<FloatVar> all = new ArrayList<FloatVar>();
		all.add(x1);
		all.add(x2);
		all.add(y1);
		all.add(y2);
		all.add(util);
		SplitSelectFloat<FloatVar> s = new SplitSelectFloat<FloatVar>(store, all.toArray(new FloatVar[] {}), null);

		search.setSolutionListener(new PrintOutListener<FloatVar>());
		search.getSolutionListener().searchAll(true);
		search.labeling(store, s);
	}

	public static void main(String[] args) {
		new TaxShockOneByOne(14.0, 8.0, 10.0, 6.0, 0.8).solve();
	}

}
