package com.agentecon.equilibrium;

import org.jacop.core.Store;
import org.jacop.floats.constraints.PmulCeqR;
import org.jacop.floats.core.FloatVar;

public class ScaledFirm implements IFirm {

	private IFirm wrapped;
	private FloatVar scaledDividend;
	private FloatVar scaledOutput;
	private FloatVar[] scaledInputs;

	public ScaledFirm(Store store, IFirm wrapped, double size) {
		this.wrapped = wrapped;
		this.scaledDividend = new FloatVar(store, wrapped.getType() + "_tot_dividend", 0.0, Double.MAX_VALUE);
		store.impose(new PmulCeqR(wrapped.getDividend(), size, scaledDividend));
		this.scaledOutput = new FloatVar(store, wrapped.getType() + "_tot_output", 0.0, Double.MAX_VALUE);
		store.impose(new PmulCeqR(wrapped.getOutput(), size, scaledOutput));
		
		FloatVar[] inputs = wrapped.getInputs();
		this.scaledInputs = new FloatVar[inputs.length];
		String type = wrapped.getType();
		for (int i=0; i<inputs.length; i++){
			this.scaledInputs[i] = new FloatVar(store, type + "_tot_input_" + i, 0.0, Double.MAX_VALUE);
			store.impose(new PmulCeqR(inputs[i], size, scaledInputs[i]));
		}
	}

	@Override
	public void imposeConstraints(FloatVar outputPrice, FloatVar... inputPrices) {
		wrapped.imposeConstraints(outputPrice, inputPrices);
	}

	@Override
	public FloatVar getDividend() {
		return scaledDividend;
	}

	@Override
	public FloatVar getOutput() {
		return scaledOutput;
	}

	@Override
	public FloatVar[] getInputs() {
		return scaledInputs;
	}

	@Override
	public String getType() {
		return wrapped.getType();
	}

}
