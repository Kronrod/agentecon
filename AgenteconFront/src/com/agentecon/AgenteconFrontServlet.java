package com.agentecon;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.modules.ModulesService;
import com.google.appengine.api.modules.ModulesServiceFactory;

@SuppressWarnings("serial")
public class AgenteconFrontServlet extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		resp.setContentType("text/html");
		resp.getWriter().println("<body>");
		resp.getWriter().println("Hello, frontend");
		ModulesService service = ModulesServiceFactory.getModulesService();
		String backendAddress = service.getVersionHostname("back", null);
		resp.getWriter().println("<a href=\"http://" + backendAddress + "/\">Backend Interface</a>");
		resp.getWriter().println("</body>");
	}
}
