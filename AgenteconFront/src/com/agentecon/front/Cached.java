package com.agentecon.front;

import java.io.IOException;

public abstract class Cached<T> {
	
	private T object;
	private int timeout;
	private long expiration;
	
	public Cached(int timeout) {
		this.timeout = timeout;
		this.expiration = 0;
	}

	public synchronized T get() throws IOException, InterruptedException {
		long now = System.currentTimeMillis();
		if (now > expiration){
			object = create();
			expiration = now + timeout;
		}
		return object;
	}

	protected abstract T create() throws IOException, InterruptedException;
	
}
