package com.agentecon.front;

import java.io.IOException;
import java.util.HashMap;

import com.agentecon.data.GitBasedSimulationList;
import com.agentecon.data.GitSimulationHandle;
import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;

/**
 * Add your first API methods in this class, or you may create another class. In that case, please update your web.xml accordingly.
 **/
@Api(name = "myApi", version = "v1", namespace = @ApiNamespace(ownerDomain = "agentecon.com", ownerName = "Agent Economics", packagePath = "") )
public class YourFirstAPI {
	
	public static final String USER = "kronrod";
	public static final String REPO = "agentecon";
	
	private HashMap<String, GitSimulationHandle> tagCache = new HashMap<String,GitSimulationHandle>();
	
	private Cached<GitBasedSimulationList> cached = new Cached<GitBasedSimulationList>(30000) {
		
		@Override
		protected GitBasedSimulationList create() throws IOException, InterruptedException {
			return new GitBasedSimulationList(tagCache, USER, REPO);
		}

	};


	@ApiMethod(name = "getSims")
	public GitBasedSimulationList getSims() throws IOException, InterruptedException {
		return cached.get();
	}

}
