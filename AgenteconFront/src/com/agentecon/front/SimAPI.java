package com.agentecon.front;

import java.io.IOException;

import com.agentecon.data.GitSimulationHandle;
import com.agentecon.data.Init;
import com.agentecon.data.SimulationInfo;
import com.agentecon.metric.series.Chart;
import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.google.api.server.spi.config.Named;
import com.google.appengine.api.modules.ModulesServiceFactory;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.util.Closeable;

/**
 * Add your first API methods in this class, or you may create another class. In that case, please update your web.xml accordingly.
 **/
@Api(name = "simApi", version = "v1", namespace = @ApiNamespace(ownerDomain = "agentecon.com", ownerName = "Agent Economics", packagePath = "") )
public class SimAPI {

	{
		Init.init();
	}

	public void triggerRun(@Named("id") String id) throws IOException, InterruptedException {
		try (Closeable c = ObjectifyService.begin()) {
			GitSimulationHandle handle = new GitSimulationHandle(YourFirstAPI.USER, YourFirstAPI.REPO, id);
			Objectify ofy = ObjectifyService.ofy();
			SimulationInfo info = ofy.load().key(Key.create(SimulationInfo.class, handle.getId())).now();
			if (info != null) {
				info.triggerRun();
				ofy.save().entity(info);
			}
			if (info == null || info.shouldRun()) {
				postWorkerTask(id);
			}
		}
	}

	@ApiMethod(name = "getHandle")
	public SimulationInfo getHandle(@Named("id") String id) throws IOException, InterruptedException {
		try (Closeable c = ObjectifyService.begin()) {
			GitSimulationHandle handle = new GitSimulationHandle(YourFirstAPI.USER, YourFirstAPI.REPO, id);
			Objectify ofy = ObjectifyService.ofy();
			SimulationInfo info = ofy.load().key(Key.create(SimulationInfo.class, handle.getId())).now();
			if (info == null) {
				info = new SimulationInfo(handle);
			} else {
				if (info.isMaster()) {
					info.update(handle);
				}
			}
			if (info.shouldRun()) {
				postWorkerTask(id);
			}
			return info;
		}
	}

	private void postWorkerTask(String id) {
		Queue queue = QueueFactory.getDefaultQueue();
		TaskOptions opt = TaskOptions.Builder.withUrl("/worker");
		opt = opt.param("user", YourFirstAPI.USER).param("repo", YourFirstAPI.REPO).param("name", id);
		opt = opt.header("Host", ModulesServiceFactory.getModulesService().getVersionHostname("back", null));
		queue.add(opt);
	}

	@ApiMethod(name = "getChart")
	public Chart getChart(@Named("id") String id) {
		long parsedId = Long.parseLong(id);
		try (Closeable c = ObjectifyService.begin()) {
			Objectify ofy = ObjectifyService.ofy();
			Chart chart = ofy.load().key(Key.create(Chart.class, parsedId)).now();
			return chart;
		}
	}

	// @ApiMethod(name = "getScaledChart")
	// public Chart getScaledChart(@Named("id") String id, @Named("maxpoints") int maxpoints) {
	// long parsedId = Long.parseLong(id);
	// try (Closeable c = ObjectifyService.begin()) {
	// Objectify ofy = ObjectifyService.ofy();
	// Chart chart = ofy.load().key(Key.create(Chart.class, parsedId)).now();
	// chart.scale(maxpoints);
	// return chart;
	// }
	// }

}
