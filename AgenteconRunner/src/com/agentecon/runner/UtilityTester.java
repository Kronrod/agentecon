package com.agentecon.runner;

import com.agentecon.metric.StockMarketStats;
import com.agentecon.metric.UtilityStats;
import com.agentecon.sim.Simulation;

public class UtilityTester {

	UtilityStats stats;

	public UtilityTester() {
		this.stats = new UtilityStats();
		StockMarketStats.PRINT_TICKER = true;
	}

	private void runAll() {
		run(new Simulation());
		// run(new Simulation(new VolumeTraderConfiguration(13, 18)));
		// for (int i = 0; i < 10; i++) {
		// run(new Simulation(new SavingConsumerConfiguration(13)));
		// }
		// run(new Simulation(new SavingFirmConfiguration(13, 2.36)));
	}

	private void run(Simulation simulation) {
		while (simulation != null) {
//			simulation.addListener(stats);
//			stats.notifySimStarting(simulation);
			StockMarketStats stocks = new StockMarketStats(simulation);
			simulation.addListener(stocks);
			stocks.notifySimStarting(simulation);
			long t0 = System.nanoTime();
			simulation.run();
			long t1 = System.nanoTime();
			System.out.println(stats.getScore().getCurrent() + "\t" + simulation.getComment() + "\t" + (t1 - t0) / 1000000 + "ms");
//			stats.notifySimEnded(simulation);
			stocks.notifySimEnded(simulation);
			simulation = (Simulation) simulation.getNext();
		}
	}

	public static void main(String[] args) {
		new UtilityTester().runAll();
	}
	
}
