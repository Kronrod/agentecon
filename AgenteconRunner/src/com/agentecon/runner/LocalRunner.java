package com.agentecon.runner;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

import com.agentecon.metric.series.Correlator;
import com.agentecon.sim.Simulation;

public class LocalRunner {

	public static void main(String[] args) throws IOException {
//		StockMarketStats.PRINT_TICKER = true;
		SimulationRunner.PRINT_CORRELATIONS = false;
		long t0 = System.nanoTime();
		SimulationRunner r = new SimulationRunner(new Simulation());
		try {
			r.run(null);
		} finally {
			System.out.println(r.getSystemOutput());
		}
		long t1 = System.nanoTime();
		long diff = (t1 - t0) / 1000000;
		System.out.println("Took " + diff + "ms to run");
		r.getCharts(13); // for testing
		Correlator c = new Correlator(r.getLatestRunTimeSeries(), 3000);
		Writer writer = new BufferedWriter(new FileWriter(new File("output.txt")));
		try {
			c.printFullTable(writer, 0);
			writer.flush();
		} finally {
			writer.close();
		}
		System.out.println("Default Correlations");
		System.out.println(c.getCorrelationMatrix());
		System.out.println("5-day averaged correlations");
		System.out.println(c.buildMovingAverage(5).getCorrelationMatrix());
		System.out.println("10-day averaged correlations");
		System.out.println(c.buildMovingAverage(10).getCorrelationMatrix());
		System.out.println("30-day averaged correlations");
		System.out.println(c.buildMovingAverage(30).getCorrelationMatrix());
		System.out.println("100-day averaged correlations");
		System.out.println(c.buildMovingAverage(100).getCorrelationMatrix());
		System.out.println("200-day averaged correlations");
		System.out.println(c.buildMovingAverage(200).getCorrelationMatrix());
		System.out.println("300-day averaged correlations");
		System.out.println(c.buildMovingAverage(300).getCorrelationMatrix());
		long t2 = System.nanoTime();
		long diff2 = (t2 - t1) / 1000000;
		System.out.println("Took " + diff2 + "ms to correlate");
	}

}
