package com.agentecon.finance;

import com.agentecon.api.IAgent;

public interface IShareholder extends IAgent {
	
	public Portfolio getPortfolio();
	
}
