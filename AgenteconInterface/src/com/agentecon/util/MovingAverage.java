// Created on Jun 1, 2015 by Luzius Meisser

package com.agentecon.util;

public class MovingAverage implements Cloneable, IAverage {
	
	private double factor;
	private double moment1, moment2;
	
	public MovingAverage(){
		this(0.95);
	}

	public MovingAverage(double d) {
		this.factor = d;
		this.moment1 = 1;
		this.moment2 = 1;
	}
	
	public double getAverage(){
		return moment1;
	}
	
	public double getVariance(){
		double var = moment2 - moment1 * moment1;
		assert var >= 0;
		return var;
	}
	
	public void add(double point){
		assert !Double.isNaN(point);
		this.moment1 = (1-factor) * point  + factor * moment1;
		this.moment2 = Math.max(moment1 * moment1, (1-factor) * (point*point)  + factor * moment2); // prevent variance from becoming negative due to rounding
		assert !Double.isNaN(moment1);
		assert !Double.isNaN(moment2);
	}
	
	public String normalize(double f) {
		return getAverage() / f + " (" + Math.sqrt(getVariance())/f + ")";
	}
	
	@Override
	public MovingAverage clone(){
		try {
			return (MovingAverage) super.clone();
		} catch (CloneNotSupportedException e) {
			throw new java.lang.RuntimeException(e);
		}
	}
	
	public String toString(){
		return getAverage() + " (" + Math.sqrt(getVariance()) + ")";
	}

}
