package com.agentecon.good;

public interface IStock {

	public Good getGood();

	public double getAmount();

	public double consume();

	public void remove(double quantity);

	public void add(double quantity);
	
	/**
	 * Will be protected from overnight depreciation
	 */
	public void addFreshlyProduced(double quantity);

	public void transfer(IStock source, double amount);

	public void absorb(IStock s);

	/**
	 * Returns a reference to this stock with an absolute part of the amount hidden
	 */
	public IStock hide(double amount);

	public boolean isEmpty();

	public void deprecate();
	
	public IStock duplicate();

	
}