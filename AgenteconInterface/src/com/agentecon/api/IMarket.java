// Created on Jun 23, 2015 by Luzius Meisser

package com.agentecon.api;

import com.agentecon.metric.IMarketListener;

public interface IMarket {
	
	public void addMarketListener(IMarketListener listener);

}
