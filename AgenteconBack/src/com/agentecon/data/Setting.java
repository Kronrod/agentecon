// Created on Jun 8, 2015 by Luzius Meisser

package com.agentecon.data;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Setting {

	private static final String GET = "get";
	
	private String name;
	private String value;
	
	public Setting(Method m, Object o) throws NotASettingException{
		String methodName = m.getName();
		if (methodName.startsWith(GET)){
			try {
				name = methodName.substring(GET.length());
				value = m.invoke(o).toString();
			} catch (IllegalAccessException e) {
				throw new java.lang.RuntimeException(e);
			} catch (IllegalArgumentException e) {
				throw new java.lang.RuntimeException(e);
			} catch (InvocationTargetException e) {
				throw new java.lang.RuntimeException(e);
			}
		} else {
			throw new NotASettingException();
		}
	}
	
}
