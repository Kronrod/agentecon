package com.agentecon.data;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class GitBasedSimulationList {

	private static final String[] HARDCODED_LIST = new String[] { "ComputationalEconomicsPaper", "ExplorationChart", "Tatonnement", "Smoothing", "Savings10", "Savings05", "Savings15",
			"FundamentalistBubble", "Longterm", "LongtermWithRetirementAt500", "Longterm004-100", "Longterm006", "Benchmark11", "Benchmark12", "Benchmark13", "Benchmark21", "Benchmark22",
			"Benchmark23", "Benchmark31", "Benchmark32", "Benchmark33" };

	private static final String NAME = "\"name\":\"";

	private ArrayList<GitSimulationHandle> sims;

	public GitBasedSimulationList(HashMap<String, GitSimulationHandle> cache, String account, String repo) throws IOException, InterruptedException {
		this.sims = new ArrayList<>();
		// this.sims.add(new GitSimulationHandle(account, repo, "master"));
		ArrayList<GitHandleFuture> futures = new ArrayList<>();
		for (String name : HARDCODED_LIST) {
			GitSimulationHandle handle = cache.get(name);
			if (handle == null) {
				futures.add(new GitHandleFuture(account, repo, name));
			} else {
				sims.add(handle);
			}
		}

		for (GitHandleFuture f : futures) {
			GitSimulationHandle handle = f.get();
			if (handle.getDate() == null) {
				continue;
			}
			cache.put(handle.getName(), handle);
			sims.add(handle);
		}

		// String content = WebUtil.readHttp("https://api.github.com/repos/" + account + "/" + repo + "/tags");
		// int pos = content.indexOf(NAME);
		// while (pos >= 0) {
		// int nameEnd = content.indexOf('"', pos + NAME.length());
		// String name = content.substring(pos + NAME.length(), nameEnd);
		// GitSimulationHandle handle = cache.get(name);
		// if (handle == null){
		// handle = new GitSimulationHandle(account, repo, name);
		// cache.put(name, handle);
		// }
		// sims.add(handle);
		// pos = content.indexOf(NAME, pos + NAME.length());
		// }
		// Collections.sort(sims, new Comparator<GitSimulationHandle>() {
		//
		// @Override
		// public int compare(GitSimulationHandle o1, GitSimulationHandle o2) {
		// return -o1.getDate().compareTo(o2.getDate());
		// }
		//
		// });
	}

	public ArrayList<GitSimulationHandle> getSims() {
		return sims;
	}

	public String toString() {
		return sims.toString();
	}

	public static void main(String[] args) throws IOException, InterruptedException {
		GitBasedSimulationList list = new GitBasedSimulationList(new HashMap<String, GitSimulationHandle>(), "kronrod", "Agentecon");
		System.out.println(list);
	}

}
