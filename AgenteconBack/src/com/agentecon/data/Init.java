// Created on Jun 8, 2015 by Luzius Meisser

package com.agentecon.data;

import com.agentecon.metric.series.Chart;

public class Init {
	
	public static void init(){
		com.googlecode.objectify.ObjectifyService.register(SimulationJar.class);
		com.googlecode.objectify.ObjectifyService.register(SimulationInfo.class);
		com.googlecode.objectify.ObjectifyService.register(Chart.class);
	}

}
