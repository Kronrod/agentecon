// Created on Jun 8, 2015 by Luzius Meisser

package com.agentecon.data;

import com.agentecon.runner.Checksum;
import com.agentecon.runner.SimulationLoader;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;

@Entity
public class SimulationJar {

	@Id
	private Long id;

	private long date;
	private String name;
	private byte[] rawData;
	private Checksum check;
	
	@SuppressWarnings("unused")
	private SimulationJar(){
	}

	public SimulationJar(long date, byte[] data) {
		this.rawData = data;
		this.name = new SimulationLoader(data).findName();
		this.date = date == 0 ? System.currentTimeMillis() : date;
		this.check = new Checksum(data);
	}

	public SimulationLoader getClassLoader() {
		return new SimulationLoader(rawData);
	}
	
	public Checksum getFingerprint() {
		return check;
	}

	public long getDate() {
		return date;
	}

	public String getName() {
		return name;
	}
	
	public static final SimulationJar load(long id){
		Key<SimulationJar> key = Key.create(SimulationJar.class, id);
		return ObjectifyService.ofy().load().key(key).now();
	}

}
