// Created on Jun 8, 2015 by Luzius Meisser

package com.agentecon.data;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;

import com.agentecon.runner.SimulationLoader;

public class WebSimulationJar {

	private byte[] rawData;

	public WebSimulationJar(URL url) throws IOException, InterruptedException {
		while (true) {
			try (InputStream input = url.openStream()) {
				this.rawData = WebUtil.readData(input);
				break;
			} catch (SocketTimeoutException e) {
				e.printStackTrace();
				Thread.sleep(5000);
			}
		}
	}

	public SimulationLoader getClassLoader() {
		return new SimulationLoader(rawData);
	}

	public static void main(String[] args) throws MalformedURLException, IOException, InterruptedException {
		String url = "http://github.com/kronrod/agentecon/raw/79698e1ebfda46f05c72000ca6908efdc230d093/jar/simulation.jar";
		System.out.println(new WebSimulationJar(new URL(url)) + " loaded");
	}

}
