// Created on Jun 24, 2015 by Luzius Meisser

package com.agentecon.runner;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

public class Checksum {

	private byte[] checksum;
	
	@SuppressWarnings("unused")
	private Checksum(){
	}
	
	public Checksum(byte[] data){
		long t0 = System.nanoTime();
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-256");
			this.checksum = md.digest(data);
		} catch (NoSuchAlgorithmException e) {
			throw new java.lang.RuntimeException(e);
		} finally {
			long t1 = System.nanoTime();
			long ys = (t1 - t0) / 1000;
			System.out.println("Generated checksum for " + data.length + " bytes in " + ys + " ys");
		}
	}
	
	public boolean equals(Object o){
		Checksum ot = (Checksum)o;
		if (ot == null){
			return false;
		} else {
			return Arrays.equals(checksum, ot.checksum);
		}
	}
	
}
