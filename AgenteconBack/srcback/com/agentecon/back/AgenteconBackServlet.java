package com.agentecon.back;

import java.io.IOException;

import javax.servlet.http.*;

import com.agentecon.api.ISimulation;

@SuppressWarnings("serial")
public class AgenteconBackServlet extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		resp.setContentType("text/plain");
		resp.getWriter().println("Hello, backend");
		resp.getWriter().println("Class ISimulation: " + ISimulation.class.getSimpleName());
	}
}
